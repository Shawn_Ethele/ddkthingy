﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZipFiles
{
    class IpscVersion
    {
        string softwareVersion;
        string ddkVersion;
        string version;
        public string SoftwareVersion { get => softwareVersion; set => softwareVersion = value; }
        public string DdkVersion { get => ddkVersion; set => ddkVersion = value; }
        public string Version { get => version; set => version = value; }
    }
}
