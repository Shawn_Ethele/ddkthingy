﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Compression;
using System.Diagnostics;
using System.IO;
using System.Security;
using Newtonsoft.Json;
using System.Net;

namespace ZipFiles
{
    public partial class Form1 : Form
    {
        IpscCollection collection;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
            collection = ParseJSON();
            if (collection == null)
                GetJSON();
            this.Text =string.Format("Using list version {0}", collection.IpscVersions[0].Version);
        }



        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Clear();

            openFileDialog1.Filter = "Driver Files (*.ipscdriver)|*.ipscdriver";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    string filePath = openFileDialog1.FileName;
                    getInfo(filePath,openFileDialog1.SafeFileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                   
                }
            }
        }

        void getInfo(string _filePath,string _fileName)
        {
            string zipPath = _filePath ;
            string extractPath = Application.StartupPath + "\\" + _fileName;
            bool found = false;
            try
            {
                textBox1.AppendText(string.Format("Loaded => {0}\r\n", _fileName ));
                textBox1.AppendText(string.Format("************************************************************************************************************************* \r\n"));
                Directory.CreateDirectory(extractPath);
                using (ZipArchive archive = ZipFile.OpenRead(zipPath))
                {                   
                    archive.ExtractToDirectory(extractPath);
                }
                string test = FileVersionInfo.GetVersionInfo(extractPath + "\\CNL.IPSecurityCenter.Driver.dll").ProductVersion.ToString();
                textBox1.AppendText(string.Format("File Targets DDK Version {0} \r\n", test));
                textBox1.AppendText(string.Format("************************************************************************************************************************* \r\n"));
                for (int a = 0; a < collection.IpscVersions.Count(); a++)
                {

                    if (collection.IpscVersions[a].DdkVersion == test)
                    {
                        found = true;
                       // Console.WriteLine(string.Format("Software Version {0} using DDK Version {1}", collection.IpscVersions[a].SoftwareVersion, collection.IpscVersions[a].DdkVersion));
                        textBox1.AppendText(string.Format("This version was packaged with IPSC version {0} and uses DDK Version {1} \r\n", collection.IpscVersions[a].SoftwareVersion, collection.IpscVersions[a].DdkVersion));
                        textBox1.AppendText(string.Format("************************************************************************************************************************* \r\n"));                        
                       
                    }
                }
                if(!found)
                    textBox1.AppendText(string.Format("No Valid Software / DDK listing found, try updating the list.\r\n"));




                if (Directory.Exists(extractPath))                
                    Directory.Delete(extractPath,true);                
                }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                if (Directory.Exists(extractPath))
                    Directory.Delete(extractPath, true);

            }
        }

        private void OpenFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {

            collection =  ParseJSON();

            for (int a = 0; a < collection.IpscVersions.Count(); a++)
            {
                Console.WriteLine(string.Format("Software Version {0} using DDK Version {1}", collection.IpscVersions[a].SoftwareVersion, collection.IpscVersions[a].DdkVersion));
            }
        }

      IpscCollection ParseJSON()
        {

            IpscCollection ipscCollection;
            try
            {
                using (StreamReader r = new StreamReader(Application.StartupPath + "\\ddklist.json"))
                {
                    string rawJSON = r.ReadToEnd();

                    ipscCollection = JsonConvert.DeserializeObject<IpscCollection>(rawJSON);
                    Console.WriteLine(ipscCollection.IpscVersions);

                }

                return ipscCollection;
            }
            catch { return null; }

        }

        private void Button2_Click_1(object sender, EventArgs e)
        {
            UpdateJSON();
        }

        private void UpdateJSON()
        {
            try
            {
                using (var webClient = new WebClient())
                {

                    string rawJSON = webClient.DownloadString("http://iot.ethele.co.za/ddk/ddklist.json");
                    IpscCollection ipscCollection = JsonConvert.DeserializeObject<IpscCollection>(rawJSON);
                   
                    if (File.Exists(Application.StartupPath + "\\ddklist.json"))
                    {
                         File.Move(Application.StartupPath + "\\ddklist.json", Application.StartupPath + "\\old_ddklist.json");
                         File.WriteAllText(Application.StartupPath + "\\ddklist.json", rawJSON);
                        File.Delete(Application.StartupPath + "\\old_ddklist.json");
                        textBox1.AppendText(string.Format("Updated list.\r\nVersion {0} loaded\r\n", ipscCollection.IpscVersions[0].Version));
                        collection = ipscCollection;
                        this.Text = string.Format("Using list version {0}", collection.IpscVersions[0].Version);
                    }
                    else
                    {
                        MessageBox.Show("Unable to download updated list, reverting to old list");
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Cannot Update");
            }
        }
        private void GetJSON()
        {
            try
            {
                using (var webClient = new WebClient())
                {

                    string rawJSON = webClient.DownloadString("http://iot.ethele.co.za/ddk/ddklist.json");
                    IpscCollection ipscCollection = JsonConvert.DeserializeObject<IpscCollection>(rawJSON);

                   
                        File.WriteAllText(Application.StartupPath + "\\ddklist.json", rawJSON);
                        collection = ipscCollection;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Cannot get JSON");
            }
        }

        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(linkLabel1.Text);
        }
    }
}
