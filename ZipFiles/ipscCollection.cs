﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZipFiles
{
    class IpscCollection
    {
        private List<IpscVersion> ipscVersions;

        public List<IpscVersion> IpscVersions { get => ipscVersions; set => ipscVersions = value; }
    }
}
